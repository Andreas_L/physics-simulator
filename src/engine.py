import numpy as np
import pygame
from circle_mass import CircleMass
import time

no_of_obj = 5
mass = 10 # Kg
radius = 0.5 # m
res = 0.01 # 10 pixels = 1m
x_max = 5
y_max = 5
dT = 0.001
exp_radius = 1
exp_Force = 20

# Create list of objects
#obj_list = [CircleMass(5, 20, mass, radius), CircleMass(10, 10, mass, radius), CircleMass(20, 20, mass, radius), CircleMass(30, 30, mass, radius), CircleMass(40, 40, mass, radius)]
obj_list = [CircleMass(2, 4, mass, radius)]
pygame.init()

# Create a canvas based on resolution and max x/y values
canvas_w = int(x_max / res)
canvas_h = int(y_max / res)

screen = pygame.display.set_mode([canvas_w, canvas_h])

def coord_to_pixel(x, y):
    # Assuming (0, 0) is the lower-left corner (for image origo is upper left)
    pixel_x = int(x / res)
    pixel_y = int(canvas_h - (y / res))

    return pixel_x, pixel_y

def pixel_to_coord(px, py):
    x = px * res
    y = (canvas_h - py) * res
    return x, y

def plot_circles():
    # Fill the background with white
    screen.fill((255, 255, 255))
    px_0, py_0 = coord_to_pixel(0, 0)
    px_1, py_1 = coord_to_pixel(1, 0)
    px_2, py_2 = coord_to_pixel(0, 1)
    pygame.draw.circle(screen, (0, 0, 255), (px_0, py_0), 10)
    pygame.draw.circle(screen, (0, 255, 0), (px_1, py_1), 10)
    pygame.draw.circle(screen, (255, 0, 0), (px_2, py_2), 10)
    for obj in obj_list:
        px, py = coord_to_pixel(obj.getX(), obj.getY())
        if obj.plot:
            pygame.draw.circle(screen, (0, 0, 255), (px, py), int(obj.radius / res))
    # Flip the display
    pygame.display.flip()

running = True
plot_circles()
while running:
    
    # Did the user click the window close button?
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

        if event.type == pygame.MOUSEBUTTONUP:
            pos = pygame.mouse.get_pos()
            # get a list of all sprites that are under the mouse cursor
            clicked_sprites = [s for s in sprites if s.rect.collidepoint(pos)]
            # do something with the clicked sprites...

    for obj in obj_list:
        obj.motion(dT, x_max)
    plot_circles()
    time.sleep(dT)

pygame.quit()