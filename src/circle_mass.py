import numpy as np

class CircleMass:
    
    A = np.array([[0, 0, 1, 0], [0, 0, 0, 1], [0, 0, 0, 0], [0, 0, 0, 0]], np.float)
    D_init = np.array([[0, 0], [0, 0], [1, 0], [0, 1]])
    g = 9.82 /2 
    def __init__(self, x, y, m, r):
        self.state = np.array([x, y, 0, 0], np.float).T
        self.mass = m
        self.radius = r
        self.B = np.array([[0, 0], [0, 0], [1 / m, 0], [0, 1/m]])
        self.D = self.D_init
        self.gravity = np.array([0, -self.g], np.float).T
        self.F = np.array([0, 0], np.float).T
        self.plot = True
        
    def affect_force(self, F):
        self.F = self.F + F

    def motion(self, dT, x_max):
        dX = np.matmul(self.A, self.state) + np.matmul(self.B, self.F) + np.matmul(self.D, self.gravity)
        self.state = self.state + dX * dT
        self.F = np.array([0, 0], np.float).T
        # If touching ground, 'bounce'
        if self.state[1] <= self.radius:
            self.state[3] = -0.9 * self.state[3] # 10% loss of energy
            self.F = self.F + np.array([0, self.mass * self.g], np.float).T
        if (self.state[0] - self.radius) < 0 or (self.state[0] + self.radius) > x_max:
            self.plot = False
            
    def getX(self):
        return self.state[0]

    def getY(self):
        return self.state[1]

    def check_collision(pos, radius):
        dist =     
        
